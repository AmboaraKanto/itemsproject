
export function getUserInfo(token) {
  const url = "https://graph.facebook.com/v2.5/me?fields=email,name&access_token=" +token;
  return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.log(error))
}

export function getItems(firebase,user){
  firebase.database().ref('items/'+user.id).on('value', (data) =>{
    let result = data.toJSON();
    let listItemFromDB = [];
    Object.keys(result).map(function(key) {
        var item = result[key];
        item.id= key;
        listItemFromDB.push(item);
    })  
    return listItemFromDB;
  })
}