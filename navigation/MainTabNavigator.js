import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator,createAppContainer } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import AjouterScreen from '../screens/AjouterScreen';
import ListeScreen from '../screens/ListeScreen';
import ProfileScreen from '../screens/ProfilScreen';
import FicheScreen from '../screens/FicheScreen';
import ModifierScreen from '../screens/ModifierScreen';


const AddStack = createStackNavigator({
  Ajouter: AjouterScreen,
});

AddStack.navigationOptions = {
  tabBarLabel: 'Ajouter',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-add' : 'md-add'}
    />
  ),
};

const ProfileStack = createStackNavigator({
  Profil: ProfileScreen,
});

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profil',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
    />
  ),
};

const HomeStack = createStackNavigator({
  Home: ListeScreen,
  Detail : FicheScreen,
  Update: ModifierScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Liste',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-home`
          : 'md-home'
      }
    />
  ),
};

const bottomtabNav = createBottomTabNavigator({
  AddStack,
  HomeStack,
  ProfileStack,
},
{
   initialRouteName: 'HomeStack',
   tabBarOptions: {
    labelStyle: {
      fontSize: 12,
    },
    inactiveBackgroundColor: '#008e97',
    activeBackgroundColor: '#ffde59',
    activeTintColor: '#008e97',
    inactiveTintColor: '#fff',
    style: {
      backgroundColor: '#ffde59',
      color: '#fff'
    }
   }
 });

 export default createAppContainer(bottomtabNav);
