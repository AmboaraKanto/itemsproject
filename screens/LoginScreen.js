import React from 'react'
import { View,Image,StyleSheet } from 'react-native';
import FBSDK, {LoginManager,AccessToken} from 'react-native-fbsdk'
import { Button,Text } from 'react-native';
import firebase from 'firebase'
import {getUserInfo} from '../api/api'
import { connect } from 'react-redux'
import Loader from '../components/Loader';
import {config} from '../api/ConfFirebase'
import PushNotification from 'react-native-push-notification'

firebase.initializeApp(config);
class LoginScreen extends React.Component{
    constructor(props){
        super(props)
        this._dispatch = this._dispatch.bind(this)       
        this.state = {
          isLoading:true
        }; 
        const component = this
        
      }

      componentWillMount(){
        // this.setState({isLoading:false});
        const component = this;
        if(AccessToken.getCurrentAccessToken()){
          AccessToken.getCurrentAccessToken().then((accessTokenData)=>{
              if(accessTokenData.accessToken!=null){
                  getUserInfo(accessTokenData.accessToken.toString())
                      .then((result)=>{ 
                          component._dispatch(result,accessTokenData.accessToken.toString()) })
                      .catch((error=>{
                        console.log(error);
                        this.setState({isLoading:false});
                      }));
              }
          }).catch((error)=>{
              console.log(error);
              this.setState({isLoading:false});
          })
      }
      }

    _dispatch = (user,token) => {
        user.token = token;
        const action = { type: "TOGGLE_USERINFO", value: user }
        this.props.dispatch(action);
        
        firebase.database().ref('items/'+this.props.user.id).on('value', (data) =>{
          let result = data.toJSON();
          if(result){
            let itemsDB = [];
            Object.keys(result).map(function(key) {
                var item = result[key];
                item.id= key;
                itemsDB.push(item);
            })  
            if(itemsDB!=null && itemsDB.length>0){
              let randId = Math.floor(Math.random()*itemsDB.length);
              let dateNotif = new Date();
              dateNotif.setHours(10, 0, 0)
              if(dateNotif < new Date()){
                dateNotif.setDate(dateNotif.getDate() + 1);
              }
              console.log(dateNotif);
              PushNotification.cancelLocalNotifications({id: '123'});
              PushNotification.localNotificationSchedule({
                message: itemsDB[randId].description,
                date: dateNotif,
                item: itemsDB[randId],
                userInteraction: false,
                id: '123'
              });
              
            } 
          }
          this.props.navigation.navigate("App");
        })
    }

    _fbAuth(component){
        this.setState({isLoading:true})
        LoginManager.logInWithReadPermissions(['public_profile',"email"]).then(function(result){
            if(result.isCancelled){
                console.log("Login Cancelled");
            }else{
                AccessToken.getCurrentAccessToken().then((accessTokenData)=>{
                    if(accessTokenData.accessToken!=null){
                        getUserInfo(accessTokenData.accessToken.toString())
                            .then((result)=>{ 
                                component._dispatch(result,accessTokenData.accessToken.toString()) })
                            .catch((error=>{console.log(error)}));
                    }else{
                      const credential = firebase.auth.FacebookAuthProvider.credential(accessTokenData.accessToken)
                      firebase.auth().signInAndRetrieveDataWithCredential(credential).then((result)=>{
                      getUserInfo(result.accessToken.toString())
                              .then((result)=>{ 
                                  component._dispatch(result,result.accessToken.toString()) 
                              })
                              .catch((error=>{console.log(error)}));
                    },(error)=>{
                       alert("Une erreur est survenue. Veuillez reessayer ulterieurement. Code 1.");
                    })
                } })
            }
        },(error)=>{
            alert("Une erreur est survenue. Veuillez reessayer ulterieurement. Code 2.");
            this.props.navigation.navigate("App");
        })
        this.setState({isLoading:false})
    }

    renderMainForm(){
      if (!this.state.isLoading) {
        return(
              <View style={styles.mainContainer}>
                  <View style={styles.logoContainer}>
                      <Image 
                          style={styles.logo}
                          source={require('../assets/images/logo.png')} 
                      />
                  </View>
                  <View style={styles.formController}>
                      <Button 
                          title="Se connecter avec Facebook"
                          onPress={()=>this._fbAuth(this)}
                          color='#008e97'
                      />
                  </View>
                  
              </View>
        );
      }
    }

    render(){
        return(
          <View style={styles.mainContainer}>
            {this.renderMainForm()}
            <Loader isLoading={this.state.isLoading} />
          </View>
        )
    }
}
const styles = StyleSheet.create({
    formController:{
      flex: 2
    },
    logoContainer:{
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
    },
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffde59',
  }
});


const mapStateToProps = (state) => {
  return state
}
const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(LoginScreen)